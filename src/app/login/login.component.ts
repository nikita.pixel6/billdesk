import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { first } from 'rxjs/operators';
import { UserService } from '../user.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  constructor(public formBuilder: FormBuilder, private userService: UserService) { }
  passwordRegx =
    '^(?=.*[0-9])' +
    '(?=.*[a-z])(?=.*[A-Z])' +
    '(?=.*[@#$%^&+=])' +
    '(?=\\S+$).{8,20}$';
  loader = false
  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: [
        '',
        [
          Validators.required,
          Validators.pattern(
            this.passwordRegx
          ),
        ],
      ],
    });
  }
  get getControl() {
    return this.loginForm.controls;
  }

  onSubmit() {
    console.log('submitted', this.loginForm.value);
    this.loader = true;
    this.userService.login(this.loginForm.value.email, this.loginForm.value.password)
      .subscribe(
        data => {
          this.loader = false;
          console.log("data" , data);
          
          // this.router.navigate([this.returnUrl]);
        },
        error => {
          console.log("error" , error);
          this.loader = false;
        });
  }
}
