import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import {  ResetpasswordComponent } from './resetpassword/resetpassword.component'
const routes: Routes = [{
  path:'' ,component: LoginComponent
},
{
  path:'reset' ,component: ResetpasswordComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
